<?php

declare(strict_types=1);

namespace Mostad\Secret\Exception;

final class FileDoesNotExistException extends \Exception
{
    /**
     * @param string $file
     *
     * @return FileDoesNotExistException
     */
    public static function fromFile(string $file): self
    {
        return new self(sprintf('File "%s" does not exist', $file));
    }
}
