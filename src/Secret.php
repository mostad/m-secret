<?php

declare(strict_types=1);

namespace Mostad\Secret;

final class Secret
{
    /**
     * @param string $secret
     * @param string $path
     *
     * @return string
     * @throws Exception\FileDoesNotExistException
     */
    public static function read(string $secret, string $path = '/run/secrets'): string
    {
        $file = $path . '/' . $secret;

        if (!is_file($file)) {
            throw Exception\FileDoesNotExistException::fromFile($file);
        }

        return rtrim((string)file_get_contents($file));
    }
}
