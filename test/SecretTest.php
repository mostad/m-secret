<?php

declare(strict_types=1);

namespace Mostad\Test\Secret;

use Mostad\Secret\Exception\FileDoesNotExistException;
use Mostad\Secret\Secret;
use PHPStan\Testing\TestCase;

final class SecretTest extends TestCase
{
    /**
     * @var string
     */
    private $secretPath = __DIR__ . '/secrets';

    /**
     * @test
     * @throws FileDoesNotExistException
     */
    public function itShouldReturn(): void
    {
        static::assertSame(
            Secret::read('SINGLE_LINE', $this->secretPath),
            'Single line secret'
        );

        static::assertSame(
            Secret::read('MULTI_LINE', $this->secretPath),
            'Multi' . PHP_EOL . 'line' . PHP_EOL . 'secret'
        );
    }

    /**
     * @test
     * @throws FileDoesNotExistException
     */
    public function itShouldThrowException(): void
    {
        $this->expectException(FileDoesNotExistException::class);
        Secret::read('NON_EXISTING', $this->secretPath);
    }
}
